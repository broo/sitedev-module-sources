SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `source_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `{$prfx}source_type` ;

CREATE  TABLE IF NOT EXISTS `{$prfx}source_type` (
  `{$prfx}source_type_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `object_type_id` TINYINT UNSIGNED NOT NULL ,
  `name` VARCHAR(255) NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `status_id` TINYINT UNSIGNED NOT NULL DEFAULT 0 ,
  `sort_order` INT UNSIGNED NULL ,
  PRIMARY KEY (`{$prfx}source_type_id`) ,
  INDEX `fk_{$prfx}source_type_object_type_id_idx` (`object_type_id` ASC)
--  CONSTRAINT `fk_{$prfx}source_type_object_type_id`
--    FOREIGN KEY (`object_type_id` )
--    REFERENCES `{$prfx}object_type` (`{$prfx}object_type_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `source`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `{$prfx}source` ;

CREATE  TABLE IF NOT EXISTS `{$prfx}source` (
  `{$prfx}source_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `object_type_id` TINYINT UNSIGNED NOT NULL ,
  `object_id` INT UNSIGNED NOT NULL ,
  `{$prfx}source_type_id` INT UNSIGNED NOT NULL ,
  `value` VARCHAR(255) NOT NULL ,
  `title` VARCHAR(255) NULL ,
  PRIMARY KEY (`source_id`) ,
  INDEX `fk_{$prfx}source_source_type_id_idx` (`{$prfx}source_type_id` ASC) ,
  INDEX `fk_{$prfx}source_object_id_idx` (`object_id` ASC) ,
  INDEX `fk_{$prfx}source_object_type_id_idx` (`object_type_id` ASC) ,
  CONSTRAINT `fk_{$prfx}source_source_type_id`
    FOREIGN KEY (`{$prfx}source_type_id` )
    REFERENCES `{$prfx}source_type` (`{$prfx}source_type_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE
--  CONSTRAINT `fk_{$prfx}source_object_id`
--    FOREIGN KEY (`object_id` )
--    REFERENCES `{$prfx}object` (`{$prfx}object_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE,
--  CONSTRAINT `fk_{$prfx}source_object_type_id`
--    FOREIGN KEY (`object_type_id` )
--    REFERENCES `{$prfx}object_type` (`{$prfx}object_type_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE
) ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
