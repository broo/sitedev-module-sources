<?php

namespace Core\Sitedev\Sources\Model;

abstract class Type extends \App\ActiveRecord
{
    const HIDDEN    = 0;
    const PUBLISHED = 1;
    const USER      = 3;

    /**
     * @var array[array]
     */
    protected static $_statuses;

    public function __construct()
    {
        parent::__construct('source_type');

        $this->addPrimaryKey('integer');
        $this->addAttr('object_type_id', 'integer');
        $this->addAttr('name', 'string');
        $this->addAttr('title', 'string');
        $this->addAttr('status_id', 'integer');
        $this->addAttr('sort_order', 'integer');
    }

    public function create()
    {
        if (!$this->_objectTypeId) {
            $this->_objectTypeId = static::OBJECT_TYPE_ID;
        }

        return parent::create();
    }

    /**
     * @param array $_where
     * @param array $_params
     * @return self[]
     */
    public static function getList($_where = null, $_params = null)
    {
        $where = empty($_where) ? array() : $_where;
        $where['object_type_id'] = static::OBJECT_TYPE_ID;

        return parent::getList($where, $_params);
    }

    /**
     * @return array[array]
     */
    public static function getStatuses()
    {
        if (!isset(static::$_statuses)) {
            static::$_statuses = array(
                static::HIDDEN    => array('title' => 'скрыт'),
                static::PUBLISHED => array('title' => 'опубликован'),
                static::USER      => array('title' => 'пользовательский')
            );

            foreach (array_keys(static::$_statuses) as $id) {
                static::$_statuses[$id]['id'] = $id;
            }
        }

        return static::$_statuses;
    }

    public function getBackOfficeXml($_xml = array(), $_attrs = array())
    {
        $attrs = $_attrs;

        if (!isset($attrs['is_published'])) {
            $attrs['is_published'] = $this->statusId == static::PUBLISHED;
        }

        return parent::getBackOfficeXml($_xml, $attrs);
    }
}
