<?php

namespace Core\Sitedev\Sources\Model;

abstract class Source extends \App\ActiveRecord
{
    public function __construct()
    {
        parent::__construct('source');

        $this->addPrimaryKey('integer');
        $this->addAttr('object_type_id', 'integer');
        $this->addAttr('object_id', 'integer');
        $this->addAttr('source_type_id', 'integer');
        $this->addAttr('value', 'string');
        $this->addAttr('title', 'string');
    }

    public function create()
    {
        if (!$this->_objectTypeId) {
            $this->_objectTypeId = static::OBJECT_TYPE_ID;
        }

        return parent::create();
    }

    public static function getList($_where = null, $_params = null)
    {
        $where = empty($_where) ? array() : $_where;
        $where['object_type_id'] = static::OBJECT_TYPE_ID;

        return parent::getList($where, $_params);
    }

    /**
     * @param $_id
     * @return self[]
     */
    public static function getObjectSources($_id)
    {
        return static::getList(array('object_id' => $_id));
    }

    /**
     * @todo Не нравится, что записи удаляются и создаются снова при каждом
     * обновлении, не зависимо от того менялись они или нет.
     *
     * @param integer $_id
     * @param array[array] $_sources
     * @return bool
     */
    public static function updateObjectSources($_id, array $_sources)
    {
        $class = get_called_class();
        static::deleteObjectSources($_id);
        $isUpdated = false;

        foreach ($_sources as $data) {
            if ($data['value']) {
                /** @var Source $source */
                $source = new $class;
                $source->objectId = $_id;
                $source->sourceTypeId = $data['type_id'];
                $source->value = $data['value'];

                if (!empty($data['title'])) {
                    $source->title = $data['title'];
                }

                if ($source->save()) {
                    $isUpdated = true;
                }
            }
        }

        return $isUpdated;
    }

    public static function deleteObjectSources($_id)
    {
        $sources = static::getObjectSources($_id);
        foreach ($sources as $source) {
            $source->delete();
        }
    }
}
